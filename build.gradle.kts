group = "org.guivista"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.5.30"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val guiVistaVer = "0.5.0"
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("glib2")
            cinterops.create("gio2")
            cinterops.create("gtk3")
            dependencies {
                implementation("io.gitlab.gui-vista:guivista-gui:$guiVistaVer")
            }
        }
        binaries {
            executable("dnd_sample") {
                entryPoint = "org.guivista.dndSample.main"
            }
        }
    }
}
