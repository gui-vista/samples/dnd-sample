package org.guivista.dndSample

import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.gui.dragDrop.connectDragDataReceivedEvent
import io.gitlab.guiVista.gui.dragDrop.setDropDestination
import io.gitlab.guiVista.gui.dragDrop.toDragDropSelectionData
import io.gitlab.guiVista.gui.widget.display.labelWidget
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction

internal fun createLabel() = labelWidget(text = "Drop something on me!") {
    connectDragDataReceivedEvent(staticCFunction(::onDragDataReceived))
    setDropDestination(defaultFlags = GTK_DEST_DEFAULT_ALL, dragActions = GDK_ACTION_COPY)
}

private fun onDragDataReceived(
    @Suppress("UNUSED_PARAMETER") widget: CPointer<GtkWidget>,
    @Suppress("UNUSED_PARAMETER") ctx: CPointer<GdkDragContext>,
    @Suppress("UNUSED_PARAMETER") xPos: Int,
    @Suppress("UNUSED_PARAMETER") yPos: Int,
    data: CPointer<GtkSelectionData>,
    info: UInt,
    @Suppress("UNUSED_PARAMETER") time: UInt,
    @Suppress("UNUSED_PARAMETER") userData: gpointer
) {
    if (info == TEXT_TARGET_ENTRY) {
        val selectionData = data.toDragDropSelectionData()
        val txt = selectionData.fetchDataAsText()
        mainWin.updateStatus("Received Text: $txt")
    } else if (info == IMAGE_BUFFER_TARGET_ENTRY) {
        val imageBuffer = data.toDragDropSelectionData().fetchDataAsImageBuffer()
        mainWin.updateStatus("Received Image Buffer (size: ${imageBuffer?.width}x${imageBuffer?.height})")
    }
}
