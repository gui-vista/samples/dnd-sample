package org.guivista.dndSample

import glib2.G_TYPE_STRING
import glib2.gpointer
import gtk3.*
import io.gitlab.guiVista.core.Object
import io.gitlab.guiVista.core.Value
import io.gitlab.guiVista.gui.IconTheme
import io.gitlab.guiVista.gui.ImageBuffer
import io.gitlab.guiVista.gui.ListStore
import io.gitlab.guiVista.gui.dragDrop.connectDragDataGetEvent
import io.gitlab.guiVista.gui.dragDrop.toDragDropSelectionData
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import io.gitlab.guiVista.gui.tree.TreePath
import io.gitlab.guiVista.gui.widget.display.IconView
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction

private val model = ListStore.create(G_TYPE_STRING, GDK_TYPE_PIXBUF)

internal fun createIconView() = IconView.createWithModel(model).apply {
    textColumn = TEXT_COLUMN
    imageBufferColumn = IMAGE_BUFFER_COLUMN
    addItem("Item 1", "image-missing")
    addItem("Item 2", "help-about")
    addItem("Item 3", "edit-copy")
    connectDragDataGetEvent(staticCFunction(::onDragDataGet))
    enableModelDragSource(startBtnMask = GDK_BUTTON1_MASK, actions = GDK_ACTION_COPY)
}

private fun addItem(txt: String, iconName: String) {
    val (imageBuffer, _) = IconTheme.fetchDefault().loadIcon(iconName = iconName, size = 16, flags = 0u)
    val iter = TreeModelIterator.create()
    val imageBufferValue = Value.create(GDK_TYPE_PIXBUF)
    imageBufferValue.changeObject(Object.fromPointer(imageBuffer?.gdkPixbufPtr))
    model.append(iter)
    model.changeStringValue(iter = iter, column = TEXT_COLUMN, value = txt)
    model.changeValue(iter = iter, column = IMAGE_BUFFER_COLUMN, value = imageBufferValue)
    imageBufferValue.close()
    iter.close()
    imageBuffer?.close()
}

private fun onDragDataGet(
    widget: CPointer<GtkWidget>,
    @Suppress("UNUSED_PARAMETER") ctx: CPointer<GdkDragContext>,
    data: CPointer<GtkSelectionData>,
    info: UInt,
    @Suppress("UNUSED_PARAMETER") time: UInt,
    @Suppress("UNUSED_PARAMETER") userData: gpointer
) {
    val iconView = IconView.fromPointer(widget.reinterpret())
    val selectedPath = TreePath.fromPointer(iconView.selectedItems.first?.data?.reinterpret())
    val (_, selectedIter) = iconView.model.fetchIterator(selectedPath)

    if (info == TEXT_TARGET_ENTRY) {
        val value = iconView.model.fetchValue(selectedIter, TEXT_COLUMN, G_TYPE_STRING)
        data.toDragDropSelectionData().changeDataAsString(value.fetchString())
    } else if (info == IMAGE_BUFFER_TARGET_ENTRY) {
        val value = iconView.model.fetchValue(iter = selectedIter, col = IMAGE_BUFFER_COLUMN,
            valueType = GDK_TYPE_PIXBUF)
        val imageBuffer = ImageBuffer.fromPointer(value.fetchObject().objPtr?.reinterpret())
        data.toDragDropSelectionData().changeDataAsImageBuffer(imageBuffer)
    }
}
