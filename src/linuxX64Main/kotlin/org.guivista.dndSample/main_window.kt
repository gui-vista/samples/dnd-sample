package org.guivista.dndSample

import glib2.gpointer
import gtk3.GtkOrientation
import gtk3.GtkToggleButton
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.dragDrop.*
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.boxLayout
import io.gitlab.guiVista.gui.widget.button.radioButtonWidget
import io.gitlab.guiVista.gui.widget.display.statusBarWidget
import io.gitlab.guiVista.gui.window.AppWindow
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction

internal class MainWindow(app: GuiApplication) : AppWindow(app) {
    private val statusBar by lazy { statusBarWidget {
        marginBottom = 3
    } }
    private val iconView by lazy { createIconView() }
    private val dropArea by lazy { createLabel() }
    private val subLayout by lazy {
        boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL) {
            spacing = 12
            appendChild(child = iconView, fill = true, expand = true)
            appendChild(child = dropArea, fill = true, expand = true)
        }
    }
    private val btnLayout by lazy {
        boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL) {
            marginTop = 6
            spacing = 6
            appendChild(child = imageBtn, fill = true, expand = false)
            appendChild(child = textBtn, fill = true, expand = false)
        }
    }
    private val imageBtn by lazy {
        radioButtonWidget(label = "Images") {
            connectToggledEvent(staticCFunction(::onImageBtnToggled))
        }
    }
    private val textBtn by lazy {
        radioButtonWidget(label = "Text") {
            joinGroup(imageBtn)
            connectToggledEvent(staticCFunction(::onTextBtnToggled))
        }
    }

    override fun createMainLayout(): ContainerBase = boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_VERTICAL) {
        appendChild(child = subLayout, fill = true, expand = true)
        appendChild(child = btnLayout, fill = true, expand = false)
        appendChild(child = statusBar, fill = true, expand = false)
        useImageTargets()
    }

    fun useImageTargets() {
        val targets = DragDropTargetList.create()
        targets.addImageTargets(IMAGE_BUFFER_TARGET_ENTRY, true)
        dropArea.dropTargets = targets
        iconView.dragTargets = targets
        targets.close()
    }

    fun useTextTargets() {
        val targets = DragDropTargetList.create()
        targets.addTextTargets(TEXT_TARGET_ENTRY)
        iconView.dragTargets = targets
        dropArea.dropTargets = targets
        targets.close()
    }

    fun updateStatus(msg: String) {
        statusBar.push(0u, msg)
    }
}

@Suppress("UNUSED_PARAMETER")
private fun onImageBtnToggled(btn: CPointer<GtkToggleButton>, userData: gpointer) {
    mainWin.useImageTargets()
}

@Suppress("UNUSED_PARAMETER")
private fun onTextBtnToggled(btn: CPointer<GtkToggleButton>, userData: gpointer) {
    mainWin.useTextTargets()
}
