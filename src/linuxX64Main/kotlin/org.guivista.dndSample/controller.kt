package org.guivista.dndSample

internal const val TEXT_COLUMN = 0
internal const val IMAGE_BUFFER_COLUMN = 1
internal const val TEXT_TARGET_ENTRY = 0u
internal const val IMAGE_BUFFER_TARGET_ENTRY = 1u

internal lateinit var mainWin: MainWindow
