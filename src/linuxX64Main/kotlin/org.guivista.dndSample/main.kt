package org.guivista.dndSample

import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.guiApplicationActivateHandler
import io.gitlab.guiVista.io.application.Application

fun main() {
    guiApplicationActivateHandler = ::activateApplication
    GuiApplication.create(id = "org.example.dndSample").use {
        mainWin = MainWindow(this)
        assignDefaultActivateHandler()
        println("Application Status: ${run()}")
    }
}

private fun activateApplication(app: Application) {
    println("Application ID: ${app.appId}")
    mainWin.createUi {
        title = "Drag And Drop"
        visible = true
    }
}
